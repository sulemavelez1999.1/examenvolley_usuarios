package com.example.faccipmveleztoalasulemaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class DetalleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        TextView name = findViewById(R.id.mName);
        TextView username = findViewById(R.id.mUsername);
        TextView email = findViewById(R.id.Email);
        TextView phone = findViewById(R.id.mPhone);
        TextView website = findViewById(R.id.mWebsite);


        Bundle bundle = getIntent().getExtras();

        String mname = bundle.getString("name");
        String musername = bundle.getString("username");
        String memail = bundle.getString("email");
        String mphone = bundle.getString("phone");
        String mwebsite = bundle.getString("website");

        name.setText(mname);
        username.setText(musername);
        email.setText(memail);
        phone.setText(mphone);
        website.setText(mwebsite);

    }
}