package com.example.faccipmveleztoalasulemaapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class UsuariosAdapter extends RecyclerView.Adapter<UsuariosAdapter.UsuarioHolder>{

    private Context context;
    private List<Usuarios> usuariosList;

    public UsuariosAdapter(Context context , List<Usuarios> usuarios){
        this.context = context;
        usuariosList = usuarios;
    }

    @NonNull
    @Override
    public UsuarioHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item , parent , false);
        return new UsuarioHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioHolder holder, int position) {
        Usuarios usuarios = usuariosList.get(position);
        holder.name.setText(usuarios.getName());
        holder.username.setText(usuarios.getUsername());
        holder.email.setText(usuarios.getEmail());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context , DetalleActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("name" , usuarios.getName());
                bundle.putString("phone" , usuarios.getPhone());
                bundle.putString("username" , usuarios.getUsername());
                bundle.putString("email" , usuarios.getEmail());
                bundle.putString("website",usuarios.getWebsite());

                intent.putExtras(bundle);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usuariosList.size();
    }

    public class UsuarioHolder extends RecyclerView.ViewHolder {
        TextView name , username , email, phone , website;
        ConstraintLayout constraintLayout;

        public UsuarioHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.mName);
            username = itemView.findViewById(R.id.mUsername);
            email = itemView.findViewById(R.id.Email);



        }
    }
}
